from odoo import tools
from odoo import api, fields, models


class AccountReceivableReportCurrentDate(models.Model):
    _name = "account.receivable.report.current.date"
    _description = "Reporte de Cuentas por Cobrar a la Fecha"
    _auto = False
    _rec_name = 'move_line_id'
    _order = "date_maturity desc"


    move_id = fields.Many2one('account.move',string="Asiento Contable",readonly=True)
    move_line_id = fields.Many2one('account.move.line',string="Apunte Contable",readonly=True)
    date_maturity = fields.Date(string="Fecha Vencimiento",readonly=True)
    date_emission = fields.Date(string="Fecha Emisión",readonly=True)
    date = fields.Date(string="Fecha Registro",readonly=True)
    currency_id = fields.Many2one('res.currency',string="",readonly=True)
    company_id = fields.Many2one('res.company',string="Compañia",readonly=True)
    company_currency_id = fields.Many2one('res.currency',string="",readonly=True)
    balance = fields.Monetary(string="Monto",readonly=True,currency_field='company_currency_id')
    amount_currency = fields.Monetary(string="Monto en ME",readonly=True,currency_field='currency_id')
    amount_residual = fields.Monetary(string="Saldo",readonly=True,currency_field='company_currency_id')
    amount_residual_currency = fields.Monetary(string="Saldo en ME",readonly=True,currency_field='currency_id')
    journal_id = fields.Many2one('account.journal',string="Diario",readonly=True)
    partner_id = fields.Many2one('res.partner',string="Socio",readonly=True)
    account_id = fields.Many2one('account.account',string="Cuenta",readonly=True)
    prefix_code = fields.Char(string="N° Serie",readonly=True)
    invoice_number = fields.Char(string="Correlativo",readonly=True)
    fecha_actual = fields.Monetary(string="A Fecha Actual",readonly=True,currency_field='company_currency_id')
    rango_1_30 = fields.Monetary(string="1-30",readonly=True,currency_field='company_currency_id')
    rango_31_60 = fields.Monetary(string="31-60",readonly=True,currency_field='company_currency_id')
    rango_61_90 = fields.Monetary(string="61-90",readonly=True,currency_field='company_currency_id')
    rango_91_120 = fields.Monetary(string="91-120",readonly=True,currency_field='company_currency_id')
    rango_mas_antiguos = fields.Monetary(string="Más Antiguos",readonly=True,currency_field='company_currency_id')



    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""
            CREATE or REPLACE VIEW {} as (
                select 
                aml.id as id,
                aml.id as move_line_id,
                aml.move_id as move_id,
                aml.date_maturity as date_maturity,
                aml.date_emission as date_emission,
                aml.date as date,
                aml.balance as balance,
                aml.amount_currency as amount_currency,
                aml.amount_residual as amount_residual,
                aml.company_id as company_id, 
                aml.amount_residual_currency as amount_residual_currency,
                aml.currency_id as currency_id,
                aml.company_currency_id as company_currency_id,
                aml.journal_id as journal_id,
                aml.partner_id as partner_id,
                aml.account_id as account_id,
                aml.prefix_code as prefix_code,
                aml.invoice_number as invoice_number,
                
                    case when aml.date_maturity is not null then 
                        case when CURRENT_DATE <= aml.date_maturity then aml.amount_residual
                        else 0.00 end
                    else
                        case when CURRENT_DATE <= aml.date then aml.amount_residual
                        else 0.00 end
                    end fecha_actual,
                        
                    case when aml.date_maturity is not null then 
                        case when aml.date_maturity <= (current_date - interval '1 day')::DATE and 
                        aml.date_maturity >= (current_date - interval '30 day')::DATE then aml.amount_residual
                        else 0.00 end
                    else
                        case when aml.date <= (current_date - interval '1 day')::DATE and 
                        aml.date >= (current_date - interval '30 day')::DATE then aml.amount_residual
                        else 0.00 end
                    end rango_1_30,
                    
                    case when aml.date_maturity is not null then 
                        case when aml.date_maturity <= (current_date - interval '31 day')::DATE and 
                            aml.date_maturity >= (current_date - interval '60 day')::DATE then aml.amount_residual
                            else 0.00 end
                    else
                        case when aml.date <= (current_date - interval '31 day')::DATE and 
                            aml.date >= (current_date - interval '60 day')::DATE then aml.amount_residual
                            else 0.00 end 
                    end rango_31_60,
                    
                    case when aml.date_maturity is not null then 
                        case when aml.date_maturity <= (current_date - interval '61 day')::DATE and 
                            aml.date_maturity >= (current_date - interval '90 day')::DATE then aml.amount_residual
                            else 0.00 end
                    else
                        case when aml.date <= (current_date - interval '61 day')::DATE and 
                            aml.date >= (current_date - interval '90 day')::DATE then aml.amount_residual
                            else 0.00 end 
                    end rango_61_90,
                    
                    case when aml.date_maturity is not null then 
                        case when aml.date_maturity <= (current_date - interval '91 day')::DATE and 
                            aml.date_maturity >= (current_date - interval '120 day')::DATE then aml.amount_residual
                            else 0.00 end
                    else
                        case when aml.date <= (current_date - interval '91 day')::DATE and 
                            aml.date >= (current_date - interval '120 day')::DATE then aml.amount_residual
                            else 0.00 end 
                    end rango_91_120,
                            
                    case when aml.date_maturity is not null then 
                        case when aml.date_maturity <= (current_date - interval '121 day')::DATE then aml.amount_residual 
                            else 0.00 end
                    else
                        case when aml.date <= (current_date - interval '121 day')::DATE then aml.amount_residual 
                            else 0.00 end 
                    end rango_mas_antiguos
                    
                from account_move_line as aml 
                join account_move am on am.id=aml.move_id
                join account_account acac on acac.id = aml.account_id 
                where acac.internal_type ='receivable' and 
                    am.state='posted' and 
                    (aml.amount_residual != 0.00 or aml.amount_residual_currency != 0.00)
            )""".format(self._table))
