from odoo import models,api,fields


class SunatTable01(models.Model):
    _name = "sunat.table.01"
    _description = "Tabla 1 : Tipo de Medio de Pago"

    active = fields.Boolean("Activo",default=True)
    name = fields.Char("Descripción")
    code = fields.Char("Código")