{
    'name': 'Tipos de Medios de Pago SUNAT',
    'version': '1.0.0',
    'category': '',
    'license': 'AGPL-3',
    'summary': "Tipos de Medios de Pago SUNAT",
    'author': "Franco Najarro",
    'website': '',
    'depends': ['account','gestionit_pe_fe'],
    'data': [
        'security/ir.model.access.csv',
        'data/data_sunat_table_01.xml',
        'views/sunat_table_01_view.xml',
        'views/account_payment_view.xml',
        ],
    'installable': True,
}
