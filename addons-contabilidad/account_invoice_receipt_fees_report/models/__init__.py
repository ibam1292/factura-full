# -*- coding: utf-8 -*-
from . import res_company
from . import res_config_settings
from . import account_move
from . import wizard_receipt_of_fees_report