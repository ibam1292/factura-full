{
	'name': 'SUNAT PLE-Registro de Inventario Permanente Valorizado y en Unidades',
	'version': "1.0.0",
	'author': 'Franco Najarro',
	'website':'',
	'category':'',
	'depends':['account','ple_base','product','stock','gestionit_pe_fe'],
	'description':'''
		Modulo de reportes PLE de Libro Registro de Inventario Permanente Valorizado.
			> Libro Registro de Inventario Permanente Valorizado
		''',
	'data':[
		'data/data_sunat_table_05.xml',
		'security/group_users.xml',
		'security/ir.model.access.csv',
		'views/product_template_view.xml',
		'views/sunat_table_05_view.xml',
		'views/ple_permanent_inventory_valued_view.xml',
		'views/ple_permanent_inventory_valued_line_view.xml',
		'views/wizard_printer_ple_permanent_inventory_valued_view.xml',
		
	],
	'installable': True,
    'auto_install': False,
}