# -*- coding: utf-8 -*-
import calendar
from io import BytesIO, StringIO
from odoo import models, fields, api, _
from datetime import datetime, timedelta
import xlsxwriter
from odoo.exceptions import UserError , ValidationError
from itertools import *

import logging
_logger=logging.getLogger(__name__)

meses={
	'01':'Enero',
	'02':'Febrero',
	'03':'Marzo',
	'04':'Abril',
	'05':'Mayo',
	'06':'Junio',
	'07':'Julio',
	'08':'Agosto',
	'09':'Septiembre',
	'10':'Octubre',
	'11':'Noviembre',
	'12':'Diciembre'}


class WizardPrinterPlePermanentInventoryValued(models.TransientModel):
	_name='wizard.printer.ple.permanent.inventory.valued'
	_inherit='wizard.printer.ple.base'
	_description = "Modulo Formulario Impresión PLE Libro Diario"

	ple_permanent_inventory_valued_id = fields.Many2one('ple.permanent.inventory.valued',string="PLE PERMANENTE VALORIZADO Y UNIDADES FÍSICAS",
		readonly=True,required=True)

	identificador_operaciones = fields.Selection(selection=[('0','Cierre de operaciones'),('1','Empresa operativa'),('2','Cierre de libro')],
		string="Identificador de operaciones", required=True, default="1")
	identificador_libro = fields.Selection(selection='available_formats_permanent_inventory_valued_sunat', string="Identificador de libro" )
	print_order = fields.Selection(default="codigo_cuenta_desagregado")


	fecha_impresion=fields.Date(string="Fecha de Impresión manual",
		default=datetime(datetime.now().year,datetime.now().month,datetime.now().day).date())


	############################################################################
	def action_print(self):
		if ( self.print_format and self.identificador_libro and self.identificador_operaciones) :
			if self.print_format =='pdf':
				return self.print_quotation()
			else:
				return super(WizardPrinterPlePermanentInventoryValued , self).action_print()
		else:
			raise UserError(_('NO SE PUEDE IMPRIMIR , Los campos: Formato Impresión , Identificador de operaciones y Identificador de libro son obligatorios, llene esos campos !!!'))



	def available_formats_permanent_inventory_valued_sunat(self):
		formats=[
			('130100','Registro Inventario Permanente Valorizado-Detalle del inventario Valorizado'),
			('120100','Registro Inventario Permanente Unidades Físicas-Detalle inventario permanente unidades fìsicas')
			]
		return formats


	
	def print_quotation(self):
		return self.env.ref('ple_permanent_inventory_valued.report_custom_a4').with_context(discard_logo_check=True).report_action(self)

	#########################################


	def criterios_impresion(self):
		res = super(WizardPrinterPlePermanentInventoryValued,self).criterios_impresion() or []
		res += [('codigo_cuenta_desagregado','Código Cuenta Desagregado')]
		return res


	def _get_order_print(self , object):
		total=''
		if self.print_order == 'date': # ORDENAMIENTO POR LA FECHA CONTABLE
			total=sorted(object,
				key=lambda PlePermanentInventoryValuedLine:PlePermanentInventoryValuedLine.fecha_movimiento)
		elif self.print_order == 'nro_documento':
			total=sorted(object,
				key=lambda PlePermanentInventoryValuedLine:PlePermanentInventoryValuedLine.asiento_contable)
		
		return total



	def file_name(self, file_format):
		nro_de_registros = '1' if len(self.ple_permanent_inventory_valued_id.ple_permanent_inventory_valued_line_ids)>0 else '0'

		file_name = "LE%s%s%s00%s%s%s1.%s" % (self.company_id.vat, self.ple_permanent_inventory_valued_id._periodo_fiscal(),
								self.identificador_libro, self.identificador_operaciones, nro_de_registros,
								self.ple_permanent_inventory_valued_id.currency_id.code_ple or '1', file_format)
		return file_name

	###################################################

	def _init_buffer(self, output):
		# output parametro de buffer que ingresa vacio
		if self.print_format == 'xlsx':
			if self.identificador_libro == '130100':
				self._generate_xlsx_valued(output)
				return output
			elif self.identificador_libro == '120100':
				self._generate_xlsx_physical_units(output)
				return output
		elif self.print_format == 'txt':
			self._generate_txt(output)
		return output




	def _convert_object_date(self, date):
		# parametro date que retorna un valor vacio o el foramto 01/01/2100 dia/mes/año
		if date:
			return date.strftime("%d/%m/%Y")
		else:
			return ''

	########################################################
	###########################################################
	## ESTA FUNCION SE ENCARGA DE AGRUPAR EN DICCIONARIO
	## luego de agrupar en diccionarios , el contenido de cada indice es ordenado por fecha en forma creciente !!!!!!!!
	## teorema: todo conjunto de puntos tomados de una funcion creciente ,es una sucesion creciente !!! 

	def _get_arbol_productos(self):
		tuplas_productos=[]
		movimientos = self.ple_permanent_inventory_valued_id.ple_permanent_inventory_valued_line_ids
		for line in movimientos:
			tuplas_productos += [(
				line.stock_move_id.product_id.id,
				line.fecha_emision_documento_traslado or '',
				line.tipo_documento_traslado or '',
				line.numero_serie_documento_traslado or '',
				line.numero_documento_traslado or '',
				line.tipo_operacion_efectuada or '',
				line.cantidad_unidades_fisicas_ingresado or 0.00,
				line.costo_unitario_bien_ingresado or 0.00,
				line.costo_total_bien_ingresado or 0.00,
				line.cantidad_unidades_fisicas_retirado or 0.00,
				line.costo_unitario_bien_retirado or 0.00,
				line.costo_total_bien_retirado or 0.00,
				line.cantidad_unids_fisicas_saldo_final or 0.00,
				line.costo_unitario_saldo_final or 0.00,
				line.costo_total_saldo_final or 0.00,
				line.codigo_propio_existencia or '',
				line.tipo_existencia or '',
				line.descripcion_existencia or '',
				line.codigo_unidad_medida or '',
				line.stock_move_id.product_id.product_tmpl_id.categ_id.property_cost_method or '',
				line.fecha_movimiento or ''
				)]

		## 20 campos !!! . Ordenando por fecha y agrupando. El orderby solo funciona con el primer campo 

		diccionario_productos={}
		# tuplas_productos = [i[1] for i in sorted(tuplas_productos)]
		#grupos_de_productos = groupby(sorted(tuplas_productos),lambda x : x[0] )
		grupos_de_productos = groupby(tuplas_productos,lambda x : x[0] )

		for k , v in grupos_de_productos:
			#diccionario_productos[k]=sorted(list(v), key=lambda u :u[20])
			diccionario_productos[k] = list(v)


		return diccionario_productos


	###################################################


	def _generate_txt(self, output):
		if self.identificador_libro == '130100':
			for line in self.ple_permanent_inventory_valued_id.ple_permanent_inventory_valued_line_ids:
				###########################################################
				escritura="%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|\n" % (
					line.periodo,
					line.asiento_contable,
					line.m_correlativo_asiento_contable,
					line.codigo_establecimiento_anexo,
					line.codigo_catalogo_utilizado,
					line.tipo_existencia,
					line.codigo_propio_existencia,
					line.codigo_existencia_catalogo_OSCE,
					self._convert_object_date(line.fecha_emision_documento_traslado),
					line.tipo_documento_traslado,
					line.numero_serie_documento_traslado,
					line.numero_documento_traslado,
					line.tipo_operacion_efectuada,
					line.descripcion_existencia,
					line.codigo_unidad_medida,
					line.codigo_metodo_valu_exist_aplicado,
					line.cantidad_unidades_fisicas_ingresado,
					line.costo_unitario_bien_ingresado,
					line.costo_total_bien_ingresado,
					line.cantidad_unidades_fisicas_retirado,
					line.costo_unitario_bien_retirado,
					line.costo_total_bien_retirado,
					line.cantidad_unids_fisicas_saldo_final,
					line.costo_unitario_saldo_final,
					line.costo_total_saldo_final,
					line.estado_operacion )

				output.write(escritura.encode())
		elif self.identificador_libro == '120100':
			for line in self.ple_permanent_inventory_valued_id.ple_permanent_inventory_physical_line_ids:
				###########################################################
				escritura="%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|%s|\n" % (
					line.periodo,
					line.asiento_contable,
					line.m_correlativo_asiento_contable,
					line.codigo_establecimiento_anexo,
					line.codigo_catalogo_utilizado,
					line.tipo_existencia,
					line.codigo_propio_existencia,
					line.codigo_existencia_catalogo_OSCE,
					self._convert_object_date(line.fecha_emision_documento_traslado),
					line.tipo_documento_traslado,
					line.numero_serie_documento_traslado,
					line.numero_documento_traslado,
					line.tipo_operacion_efectuada,
					line.descripcion_existencia,
					line.codigo_unidad_medida,
					line.cantidad_unidades_fisicas_ingresado,
					line.cantidad_unidades_fisicas_retirado,
					line.estado_operacion )

				output.write(escritura.encode())

	####################################################################

	def _generate_xlsx_valued(self, output):
		workbook = xlsxwriter.Workbook(output)
		ws = workbook.add_worksheet('F 13.1 Det.Inv.Per.Val.')
		styles = {'font_size': 8,'font_name':'Arial','align': 'center','valign': 'center','border': 1}
		titulo_1 = workbook.add_format({'font_size': 14, 'font_name':'Arial', 'bold': True})
		titulo_2 = workbook.add_format({'font_size': 10, 'font_name':'Arial', 'bold': True})
		titulo_3 = workbook.add_format(styles)
		titulo_4 = workbook.add_format({'font_size': 8, 'font_name':'Arial', 'align': 'center', 'valign': 'center', 'border': 1, 'fg_color': '#C4C4C4'})
		ws.set_column('A:A', 15,titulo_1)
		ws.set_column('B:B', 15,titulo_1)
		ws.set_column('C:C', 13,titulo_1)
		ws.set_column('D:D', 13,titulo_1)
		ws.set_column('E:E', 13,titulo_1)
		ws.set_column('F:F', 13,titulo_1)
		ws.set_column('G:G', 15,titulo_1)
		ws.set_column('H:H', 13,titulo_1)
		ws.set_column('I:I', 13,titulo_1)
		ws.set_column('J:J', 15,titulo_1)
		ws.set_column('K:K', 13,titulo_1)
		ws.set_column('L:L', 13,titulo_1)
		ws.set_column('M:M', 15,titulo_1)
		ws.set_column('N:N', 13,titulo_1)
		ws.write(0,0,'FORMATO 13.1:', titulo_1)
		ws.write(0,1,'REGISTRO DE INVENTARIO PERMANENTE VALORIZADO - DETALLE DEL INVENTARIO VALORIZADO',titulo_1)
		ws.write(2,0,'PERIODO:', titulo_2)
		ws.write(2,1,self.ple_permanent_inventory_valued_id._periodo_fiscal(),titulo_2)
		ws.write(3,0,'RUC:', titulo_2)
		ws.write(3,1,self.company_id.vat,titulo_2)
		ws.merge_range('A5:D5','APELLIDOS Y NOMBRES, DENOMINACIÓN O RAZÓN SOCIAL:', titulo_2)
		ws.write(4,4,self.company_id.name,titulo_2)
		ws.merge_range('A6:B6','ESTABLECIMIENTO (1):', titulo_2)

		#############################################
		## dirección fiscal de la empresa
		direccion = "%s %s %s %s"%(self.company_id.street or '',self.company_id.street2 or '',
			self.company_id.state_id and self.company_id.state_id.name or '',self.company_id.country_id.name or '')
		#############################################

		ws.write(5,2,direccion,titulo_2)
		ws.merge_range('A9:D9','DOCUMENTO DE TRASLADO, COMPROBANTE DE PAGO,', workbook.add_format(dict(styles, bottom=0)))
		ws.merge_range('A10:D10','DOCUMENTO INTERNO O SIMILAR', workbook.add_format(dict(styles, top=0)))
		ws.write(10,0,'FECHA', titulo_3)
		ws.write(10,1,'TIPO (TABLA 10)', titulo_3)
		ws.write(10,2,'SERIE', titulo_3)
		ws.write(10,3,'NÚMERO', titulo_3)
		ws.write(8,4,'TIPO DE', workbook.add_format(dict(styles, bottom=0)))
		ws.write(9,4,'OPERACIÓN', workbook.add_format(dict(styles, top=0, bottom=0)))
		ws.write(10,4,'(TABLA 12)', workbook.add_format(dict(styles, top=0)))
		ws.merge_range('F9:H9','ENTRADAS', titulo_3)
		ws.merge_range('F10:F11','CANTIDAD', titulo_3)
		ws.merge_range('G10:G11','COSTO UNITARIO', titulo_3)
		ws.merge_range('H10:H11','COSTO TOTAL', titulo_3)
		ws.merge_range('I9:K9','SALIDAS', titulo_3)
		ws.merge_range('I10:I11','CANTIDAD', titulo_3)
		ws.merge_range('J10:J11','COSTO UNITARIO', titulo_3)
		ws.merge_range('K10:K11','COSTO TOTAL', titulo_3)
		ws.merge_range('L9:N9','SALDO FINAL', titulo_3)
		ws.merge_range('L10:L11','CANTIDAD', titulo_3)
		ws.merge_range('M10:M11','COSTO UNITARIO', titulo_3)
		ws.merge_range('N10:N11','COSTO TOTAL', titulo_3)
		ws.freeze_panes(12,0)
		row = 12

		data = self._get_arbol_productos()
		_logger.info('\n\nARBOL DE PRODUCTOS\n\n')
		_logger.info(data)
		
		for index in data:
			ws.merge_range("A%s:B%s"%(row+1,row+1),'CÓDIGO DE LA EXISTENCIA:', titulo_2)
			ws.write(row,2,data[index][0][15],titulo_2)
			row += 1
			ws.write(row,0,'TIPO (TABLA 5):', titulo_2)
			ws.write(row,1,data[index][0][16],titulo_2)
			row += 1
			ws.write(row,0,'DESCRIPCIÓN:', titulo_2)
			ws.write(row,1,data[index][0][17],titulo_2)
			row += 1
			ws.merge_range("A%s:C%s"%(row+1,row+1),'CÓDIGO DE LA UNIDAD DE MEDIDA (TABLA 6):', titulo_2)
			ws.write(row,3,data[index][0][18],titulo_2)
			row += 1
			ws.merge_range("A%s:B%s"%(row+1,row+1),'MÉTODO DE VALUACIÓN:', titulo_2)
			ws.write(row,2,data[index][0][19],titulo_2)
			row += 1
			for line in data[index]:
				ws.write(row,0,self._convert_object_date(line[1]),titulo_3)
				ws.write(row,1,line[2],titulo_3)
				ws.write(row,2,line[3],titulo_3)
				ws.write(row,3,line[4],titulo_3)
				ws.write(row,4,line[5],titulo_3)
				ws.write(row,5,line[6],titulo_3)
				ws.write(row,6,line[7],titulo_3)
				ws.write(row,7,line[8],titulo_3)
				ws.write(row,8,line[9],titulo_3)
				ws.write(row,9,line[10],titulo_3)
				ws.write(row,10,line[11],titulo_3)
				ws.write(row,11,line[12],titulo_3)
				ws.write(row,12,line[13],titulo_3)
				ws.write(row,13,line[14],titulo_3)
				row += 1
			ws.write(row,4,'TOTALES', titulo_2)
			ws.write(row,5,sum([i[6] for i in data[index] ]),titulo_3)
			ws.write(row,6,sum([i[7] for i in data[index] ]),titulo_4)
			ws.write(row,7,sum([i[8] for i in data[index] ]),titulo_3)
			ws.write(row,8,sum([i[9] for i in data[index] ]),titulo_3)
			ws.write(row,9,sum([i[10] for i in data[index] ]),titulo_4)
			ws.write(row,10,sum([i[11] for i in data[index] ]),titulo_3)
			ws.write(row,11,data[index][len(data[index])-1][12],titulo_4)
			ws.write(row,12,sum([i[13] for i in data[index] ]),titulo_4)
			ws.write(row,13,sum([i[14] for i in data[index] ]),titulo_4)
			row += 1

		workbook.close()


	#########################################################################
	def _generate_xlsx_physical_units(self, output):
		workbook = xlsxwriter.Workbook(output)
		ws = workbook.add_worksheet('F 13.1 Det.Inv.Per.Uni.Fis.')
		styles = {'font_size': 8,'font_name':'Arial','align': 'center','valign': 'center','border': 1}
		titulo_1 = workbook.add_format({'font_size': 14, 'font_name':'Arial', 'bold': True})
		titulo_2 = workbook.add_format({'font_size': 9, 'font_name':'Arial', 'bold': True})
		titulo_3 = workbook.add_format(styles)
		ws.set_column('A:A', 17,titulo_1)
		ws.set_column('B:B', 15,titulo_1)
		ws.set_column('C:C', 15,titulo_1)
		ws.set_column('D:D', 13,titulo_1)
		ws.set_column('E:E', 27,titulo_1)
		ws.set_column('H:H', 13,titulo_1)
		ws.write(0,1,'REGISTRO DEL INVENTARIO PERMANENTE EN UNIDADES FÍSICAS', titulo_1)
		data = self._get_arbol_productos()
		row = 2
		months = ['ENERO','FEBRERO','MARZO','ABRIL','MAYO','JUNIO','JULIO','AGOSTO','SEPTIEMBRE','OCTUBRE','NOVIEMBRE','DICIEMBRE']

		#############################################
		## dirección fiscal de la empresa
		direccion = "%s %s %s %s"%(self.company_id.street or '',self.company_id.street2 or '',
			self.company_id.state_id and self.company_id.state_id.name or '',self.company_id.country_id.name or '')
		#############################################

		for index in data:
			ws.write(row,0,'PERIODO:', titulo_2)
			ws.write(row,1,meses[self.ple_permanent_inventory_valued_id.fiscal_month] + ' ' + self.ple_permanent_inventory_valued_id.fiscal_year,titulo_2)
			row += 1
			ws.write(row,0,'RUC:', titulo_2)
			ws.write(row,1,self.company_id.vat,titulo_2)
			row += 1
			ws.merge_range("A%s:C%s"%(row+1,row+1),'APELLIDOS Y NOMBRES, DENOMINACIÓN O RAZÓN SOCIAL:', titulo_2)
			ws.write(row,3,self.company_id.name,titulo_2)
			row += 1
			ws.write(row,0,'ESTABLECIMIENTO:', titulo_2)
			ws.write(row,1,direccion,titulo_2)
			row += 1
			ws.merge_range("A%s:B%s"%(row+1,row+1),'CÓDIGO DE LA EXISTENCIA:', titulo_2)
			ws.write(row,2,data[index][0][15],titulo_2)
			row += 1
			ws.write(row,0,'TIPO:', titulo_2)
			ws.write(row,1,data[index][0][16],titulo_2)
			row += 1
			ws.write(row,0,'DESCRIPCIÓN:', titulo_2)
			ws.write(row,1,data[index][0][17],titulo_2)
			row += 1
			ws.merge_range("A%s:B%s"%(row+1,row+1),'CÓDIGO DE LA UNIDAD DE MEDIDA:', titulo_2)
			ws.write(row,2,data[index][0][18],titulo_2)
			row += 2
			ws.merge_range("A%s:D%s"%(row+1,row+1),'DOCUMENTO DE TRASLADO, COMPROBANTE DE PAGO,',workbook.add_format(dict(styles, bottom=0)))
			ws.merge_range("A%s:D%s"%(row+2,row+2),'DOCUMENTO INTERNO O SIMILAR',workbook.add_format(dict(styles, top=0)))
			ws.merge_range("E%s:E%s"%(row+1,row+3),'TIPO DE OPERACIÓN', titulo_3)
			ws.merge_range("F%s:F%s"%(row+1,row+2),'ENTRADAS', titulo_3)
			ws.merge_range("G%s:G%s"%(row+1,row+2),'SALIDAS', titulo_3)
			ws.merge_range("H%s:H%s"%(row+1,row+2),'SALDO FINAL', titulo_3)
			row += 2
			ws.write(row,0,'FECHA',titulo_3)
			ws.write(row,1,'TIPO',titulo_3)
			ws.write(row,2,'SERIE',titulo_3)
			ws.write(row,3,'NÚMERO',titulo_3)
			ws.write(row,5,'CANTIDAD',titulo_3)
			ws.write(row,6,'CANTIDAD',titulo_3)
			ws.write(row,7,'CANTIDAD',titulo_3)
			row += 1
			for line in data[index]:
				ws.write(row,0,self._convert_object_date(line[1]),titulo_3)
				ws.write(row,1,line[2],titulo_3)
				ws.write(row,2,line[3],titulo_3)
				ws.write(row,3,line[4],titulo_3)
				ws.write(row,4,line[5],titulo_3)
				ws.write(row,5,line[6],titulo_3)
				ws.write(row,6,line[9],titulo_3)
				ws.write(row,7,line[12],titulo_3)
				row += 1
			ws.write(row,4,'TOTALES PRODUCTO >>>>>', titulo_2)
			ws.write(row,5,sum([i[6] for i in data[index] ]),titulo_3)
			ws.write(row,6,sum([i[9] for i in data[index] ]),titulo_3)
			ws.write(row,7,data[index][len(data[index])-1][12],titulo_3)
			row += 2
		workbook.close()

	

	def is_menor(self,a,b):
		return a<b