import calendar
from io import BytesIO, StringIO
from odoo import models, fields, api, _
from datetime import datetime, timedelta
import xlsxwriter
from odoo.exceptions import UserError , ValidationError

import logging
_logger=logging.getLogger(__name__)

class ProductTemplate(models.Model):
	_inherit='product.template'
	
	product_encoding_type_sunat=fields.Selection(selection=[('3','GS1 (EAN-UCC)'),('9','OTROS')],
		string="Catálogo de Existencias-Codificación" , default='9')

	sunat_table_05_id = fields.Many2one('sunat.table.05',string="Tipo de Existencia SUNAT",
		domain="[('active','=',True)]")
	