import pytz
import calendar
import base64
from io import BytesIO, StringIO
from odoo import models, fields, api, _
from odoo.exceptions import UserError, ValidationError, Warning
from datetime import datetime, timedelta
from odoo.addons import ple_base as tools
from itertools import *

import logging
_logger=logging.getLogger(__name__)

class PlePermanentInventoryValuedLine(models.Model):
	_name='ple.permanent.inventory.valued.line'


	ple_permanent_inventory_valued_id=fields.Many2one("ple.permanent.inventory.valued",string="id PLE", ondelete="cascade" )
	ple_permanent_inventory_physical_id=fields.Many2one("ple.permanent.inventory.valued",string="id PLE", ondelete="cascade" )
	
	####################################################################################################
	stock_valuation_layer_id = fields.Many2one('stock.valuation.layer',string="Linea de Valorización",readonly=True)
	
	picking_id = fields.Many2one('stock.picking',string="Stock Picking",readonly=True)
	
	move_id=fields.Many2one("account.move",string="Asiento contable",readonly=True)

	stock_move_id= fields.Many2one('stock.move',string="Movimiento de Stock",readonly=True)

	periodo=fields.Char(string="Periodo PLE",readonly=True)

	invoice_number=fields.Char(string="Factura/Documento",readonly=True)

	invoice_id = fields.Many2one('account.move',string="Factura",readonly=True,
		compute="_compute_campo_invoice_id",store=True)

	fecha_movimiento=fields.Datetime(string='Fecha de Movimiento', readonly=True)

	asiento_contable=fields.Char(string="Nombre del asiento contable",readonly=True)

	m_correlativo_asiento_contable=fields.Char(string="M-correlativo asiento contable",readonly=True )
	codigo_establecimiento_anexo = fields.Char(string="Código de Establecimiento Anexo",readonly=True )
	codigo_catalogo_utilizado = fields.Char(string="Código del catálogo utilizado",readonly=True )
	tipo_existencia = fields.Char(string="Tipo Existencia",readonly=True )
	
	codigo_propio_existencia = fields.Char(string="Código propio Existencia",readonly=True )
	codigo_existencia_catalogo_OSCE = fields.Char(string="Código de Existencia de acuerdo a OSCE",readonly=True )
	
	fecha_emision_documento_traslado = fields.Date(string="Fecha Emisión Documento de Traslado",
		compute='_compute_campo_fecha_emision_documento_traslado',store=True,readonly=True )
	
	tipo_documento_traslado=fields.Char(string="Tipo de Documento de Traslado",
		compute='_compute_campo_tipo_documento_traslado',store=True,readonly=True )

	numero_serie_documento_traslado=fields.Char(string="Número de Serie Documento de Traslado",
		compute='_compute_campo_numero_serie_documento_traslado',store=True,readonly=True )

	numero_documento_traslado=fields.Char(string="Número de Documento de Traslado",
		compute='_compute_campo_numero_documento_traslado',store=True,readonly=True )

	tipo_operacion_efectuada=fields.Char(string="Tipo de Operación Efectuada",readonly=True )

	descripcion_existencia=fields.Char(string="Descripción de la Existencia",readonly=True )

	codigo_unidad_medida = fields.Char(string="Código de la unidad de Medida",readonly=True )

	codigo_metodo_valu_exist_aplicado = fields.Char(string="Código Método Valuación de Existencias Aplicado",
		compute='_compute_campo_codigo_metodo_valu_exist_aplicado'  ,store=True , readonly=True )#
	
	cantidad_unidades_fisicas_ingresado = fields.Float(string="Cantidad Unidades del Bien Ingresado",readonly=True )
	
	costo_unitario_bien_ingresado = fields.Float(string="Costo Unitario del Bien Ingresado",readonly=True )

	costo_total_bien_ingresado = fields.Float(string="Costo Total del Bien Ingresado",readonly=True )

	cantidad_unidades_fisicas_retirado = fields.Float(string="Cantidad Unidades del bien Retirado",readonly=True )

	costo_unitario_bien_retirado = fields.Float(string="Costo Unitario del Bien Retirado",readonly=True )

	costo_total_bien_retirado = fields.Float(string="Costo Total del Bien Retirado",readonly=True )

	cantidad_unids_fisicas_saldo_final = fields.Float(string="Cantidad Unidades Físicas Saldo Final",readonly=True )

	costo_unitario_saldo_final = fields.Float(string="Costo Unitario Saldo Final",readonly=True )

	costo_total_saldo_final = fields.Float(string="Costo Total del Saldo Final",readonly=True )

	estado_operacion = fields.Char(string="Estado de la Operación" , compute='_compute_campo_estado_operacion'  ,store=True , readonly=True )#

	#########################################################

	@api.depends('invoice_number')
	def _compute_campo_invoice_id(self):
		for rec in self:
			rec.invoice_id = False
			if rec.invoice_number:
				invoice_number_0 = rec.invoice_number.split(',')
				if invoice_number_0:
					invoice_number_0 = invoice_number_0[0]
					invoice_id = self.env['account.move'].search([('name','=',invoice_number_0)],limit=1)
					rec.invoice_id = invoice_id or False



	@api.depends('invoice_number')
	def _compute_campo_tipo_documento_traslado(self):
		# self.tipo_documento_traslado = self.stock_move_id.stock_picking_table10_id_fkey.code or ''
		for rec in self:
			self.tipo_documento_traslado = ''

			if rec.invoice_number:
				rec.tipo_documento_traslado = ''
				#rec.invoice_number.table10_id.code or ''
			else:
				rec.tipo_documento_traslado = ''




	@api.depends('invoice_id')
	def _compute_campo_numero_serie_documento_traslado(self):
		for rec in self:
			rec.numero_serie_documento_traslado = ''
			if rec.invoice_id:
				rec.numero_serie_documento_traslado = rec.invoice_id.prefix_code or ''



	@api.depends('invoice_id')
	def _compute_campo_numero_documento_traslado(self):
		for rec in self:
			rec.numero_documento_traslado = ''
			if rec.invoice_id:
				rec.numero_documento_traslado = rec.invoice_id.invoice_number or ''


	## LOS CODIGOS DE LA TABLA 14 DE LA SUNAT SON : 
	# 1	PROMEDIO PONDERADO
	# 2	PRIMERAS ENTRADAS, PRIMERAS SALIDAS
	# 3	EXISTENCIAS BÁSICAS
	# 4	DETALLISTA
	# 5	IDENTIFICACIÓN ESPECÍFICA
	# 9	OTROS

	# LOS CODIGOS DEL ANKA SON:
	# STANDARD (4), FIFO (2) Y AVERAGE (1)
	@api.depends('stock_move_id')
	def _compute_campo_codigo_metodo_valu_exist_aplicado(self):
		for rec in self:
			rec.codigo_metodo_valu_exist_aplicado = ''

			if rec.stock_move_id:
				method=rec.stock_move_id.product_id.product_tmpl_id.categ_id.property_cost_method
				if method == 'standard':
					rec.codigo_metodo_valu_exist_aplicado = '4'
				elif method == 'fifo':
					rec.codigo_metodo_valu_exist_aplicado = '2'
				elif method=='average':
					rec.codigo_metodo_valu_exist_aplicado = '1'
				else:
					rec.codigo_metodo_valu_exist_aplicado = ''



	@api.depends('periodo','fecha_movimiento')
	def _compute_campo_estado_operacion(self):
		# self.estado_operacion=''
		for rec in self:
			rec.estado_operacion= ''
			if rec.periodo and rec.fecha_movimiento:
				if tools.getDateYYYYMMDD(rec.fecha_movimiento) >= (rec.periodo or '') :
					rec.estado_operacion='1'
				else:
					rec.estado_operacion='8'
					
			else:
				rec.estado_operacion='1'