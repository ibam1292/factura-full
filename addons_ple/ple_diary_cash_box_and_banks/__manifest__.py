{
	'name': 'SUNAT PLE-Libro Caja Bancos',
	'version': "1.0.0",
	'author': 'Franco Najarro',
	'website':'',
	'category':'Accounting',
	'depends':['account','ple_base','bo_pe_contabilidad_documents',
		'bo_pe_payment_method_sunat','extra_account_move_line'],
	'description':'''
		Modulo de reportes PLE de Libro Caja Bancos.
			> Libro Caja Bancos
		''',
	'data':[
		'security/ir.model.access.csv',
		'views/account_journal_view.xml',
		'views/ple_diary_cash_box_and_banks_view.xml',
		'views/ple_diary_cash_box_and_banks_line_view.xml',
		'views/wizard_printer_ple_diary_cash_box_and_banks_view.xml',
	],
	'installable': True,
    'auto_install': False,
}