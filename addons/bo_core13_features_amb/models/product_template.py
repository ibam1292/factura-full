import calendar
from io import BytesIO, StringIO
from odoo import models, fields, api, _
from odoo.exceptions import UserError , ValidationError

import logging
_logger=logging.getLogger(__name__)

class ProductTemplate(models.Model):
	_inherit='product.template'
	
	product_brand_id = fields.Many2one('product.product.brand', string="Marca")	