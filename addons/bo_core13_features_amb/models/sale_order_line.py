import calendar
from io import BytesIO, StringIO
from odoo import models, fields, api, _
from odoo.exceptions import UserError , ValidationError

import logging
_logger=logging.getLogger(__name__)

class SaleOrderLine(models.Model):
	_inherit='sale.order.line'
	
	product_brand_id = fields.Many2one('product.product.brand',string="Marca",
		compute="compute_campo_product_brand_id",store=True)


	@api.depends('product_id')
	def compute_campo_product_brand_id(self):
		for rec in self:
			rec.product_brand_id = False
			if rec.product_id:
				rec.product_brand_id = rec.product_id.product_tmpl_id and \
					rec.product_id.product_tmpl_id.product_brand_id or False