{
	'name': 'Marca en Productos y Cotizaciones',
	'version': "1.0.0",
	'author': 'Franco Najarro',
	'website':'',
	'category':'',
	'depends':['stock','product','sale','gestionit_pe_fe'],
	'description':'''
		Modulo de Marca en Productos y Cotizaciones.
			> Marca en Productos y Cotizaciones.
		''',
	'data':[
		'security/ir.model.access.csv',
		'views/product_product_brand_view.xml',
		'views/product_template_view.xml',
		'views/sale_order_view.xml',
	],
	'installable': True,
    'auto_install': False,
}